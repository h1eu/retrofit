package com.example.callapi.api

import com.example.callapi.model.DataResponse
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface ApiServices {
    companion object{
        var BASE_URL = "https://ringdev.alo.vn"
        fun create() : ApiServices {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
            return retrofit.create(ApiServices::class.java)
        }
    }
    @GET("/api/hashtag")
    fun getHashTag(
        @Header("language") language: String,
        @Header("country") country : String,
        @Header("X-AppVersion") X_AppVersion: String,
        @Query("offset") offset: Int,
        @Query("limit") limit: Int
    ) : Call<DataResponse>
}