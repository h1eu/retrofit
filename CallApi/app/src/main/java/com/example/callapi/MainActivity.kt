package com.example.callapi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import androidx.databinding.DataBindingUtil
import com.example.callapi.api.ApiServices
import com.example.callapi.databinding.ActivityMainBinding
import com.example.callapi.model.DataResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {
    private lateinit var binding : ActivityMainBinding
    private lateinit var btn : Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_main)
        init()
        handle()

    }

    private fun handle() {
        btn.setOnClickListener(View.OnClickListener {
            var offset = 0
            var limit = 20
            var language = "vi"
            var country = "VN"
            var X_AppVersion = ""
            ApiServices.create().getHashTag(language,country,X_AppVersion,offset,limit).enqueue(
                object : Callback<DataResponse>{
                    override fun onResponse(call: Call<DataResponse>, response: Response<DataResponse>) {
                        if(response != null){
                            var dataResponse = response.body()
                            Log.e("success",dataResponse!!.data.get(10).name)
                        }

                    }

                    override fun onFailure(call: Call<DataResponse>, t: Throwable) {
                        Log.e("Fail","call api that bai")
                    }

                })
        })
    }

    private fun init(){
        btn = binding.btn
    }
}