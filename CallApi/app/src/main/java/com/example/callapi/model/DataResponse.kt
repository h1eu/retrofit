package com.example.callapi.model


import com.google.gson.annotations.SerializedName

data class DataResponse(
    @SerializedName("data")
    var `data`: List<Data>,
    @SerializedName("status")
    var status: Status
) {
    data class Data(
        @SerializedName("count")
        var count: Any,
        @SerializedName("countByCountry")
        var countByCountry: String,
        @SerializedName("country")
        var country: Any,
        @SerializedName("createdDate")
        var createdDate: Long,
        @SerializedName("description")
        var description: Any,
        @SerializedName("displayByLang")
        var displayByLang: String,
        @SerializedName("hashtag")
        var hashtag: String,
        @SerializedName("id")
        var id: Int,
        @SerializedName("isMin")
        var isMin: Boolean,
        @SerializedName("isShow")
        var isShow: Boolean,
        @SerializedName("isVideo")
        var isVideo: Any,
        @SerializedName("name")
        var name: String,
        @SerializedName("searchName")
        var searchName: Any,
        @SerializedName("url")
        var url: Any
    )

    data class Status(
        @SerializedName("message")
        var message: String,
        @SerializedName("statusCode")
        var statusCode: Int
    )
}